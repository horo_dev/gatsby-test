import React from "react"
import styled from "styled-components"
import { Section, Heading, media } from "@styles"
import Img from "gatsby-image"

const StyledGrid = styled.div`
display: grid;
grid-template-columns: repeat(1, minmax(0, 1fr));

${media.lg`grid-template-columns: repeat(2, minmax(0, 1fr));`}
`;


const FeaturedContainer = ({ data }) => {
    const { frontmatter } = data[0].node;
    const { title, projects } = frontmatter;

    return (
        <Section>
            <Heading>{title}</Heading>

            <StyledGrid>
                {
                    projects.map(({ title, tags, url, img, description }, i) => (
                        <div>
                            <Img fluid={img.childImageSharp.fluid} alt={title}/>
                        </div>
                    ))
                }
            </StyledGrid>
        </Section>
    );
};

export default FeaturedContainer;