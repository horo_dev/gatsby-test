import React from "react"
import PropTypes from "prop-types"
import Img from "gatsby-image"
import styled from "styled-components"
import { theme, media, Heading } from "@styles"

const { colors, margins, fontSizes, fonts } = theme;


const Skill = ({ img_src, short_name, name }) => (
    <SkillItem className="border rounded-full">
      <img className="border-b" src={img_src} alt={`Logo of ${short_name}`} />
      {name}
    </SkillItem>
)

const SkillItem = styled.li`
border-color: ${colors.border};
padding: 1rem;
display: flex;
flex-direction: column;
flex: 1 1 0%;
align-items: center;
justify-content: center;
white-space: nowrap;
overflow: hidden;
filter: grayscale(1);
transition: filter 150ms ease-out;

img {
  height: 2em;
  padding-bottom: ${margins.sm};
  margin-bottom: ${margins.xs};
}

&:hover, &:focus {
  filter: grayscale(0);
}

`
const SkillList = styled.ul`
  display: grid;
  grid-template-columns: repeat(1, minmax(0, 1fr));
  grid-template-rows: min-content;
  align-items: start;
  padding: 0;
  gap: ${margins.xs};

  ${media.sm`grid-template-columns: repeat(3, minmax(0, 1fr);`};
  ${media.md`gap: ${margins.sm};`};
`

const StyledParagraphContainer = styled.div`
p:not(:last-of-type) {
  margin-bottom: ${margins.sm};
}
a {
  color: ${colors.heading};
  font-weight: 500;
  position: relative;
  &::before {
    height: 100%;
    width: 100%;
    background-color: ${colors.border};
    position: absolute;
    bottom: 0;
    left: 0;
    content: "";
    z-index: -1;
    transform: scaleY(0.1);
    transform-origin: bottom center;

    transition: 350ms transform cubic-bezier(0.32, 1.28, 0.18, 0.89);
  }

  &:hover {
    color: ${colors.brand};
    &::before {
      transform: scaleY(.8);
    }
  }
}
`

const Headline = styled.div`
  color: ${colors.heading};
  margin-top: ${margins.lg};
  margin-bottom: ${margins.lg};
  h1 {
    font-weight: 700;
    font-size: ${fontSizes.xxl};
    margin-bottom: ${margins.lg};
  }
  h2 {
    font-size: ${fontSizes.xl};
  }
`

const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  font-family: ${fonts.sans_serif};


  padding-bottom: ${margins.xxl};
  padding-top: ${margins.lg};
`

const StyledOverline = styled.h1`
`

const StyledName = styled.h2`

`

const StyledSubtitle = styled.h3`

`

const About = ({ data }) => {
  const { frontmatter, html } = data[0].node;
  const { title, name, subtitle, avatar, skills } = frontmatter;
  
  return (
    <StyledContainer>
      <Headline>
        <Img className="w-16 h-16 rounded-full filter-brand mb-6 overflow-hidden" fixed={avatar.childImageSharp.fixed} alt="of Tim Nelke"></Img>
        <StyledOverline>{title}</StyledOverline>
        <StyledName>{name}</StyledName>
        <StyledSubtitle>{subtitle}</StyledSubtitle>
      </Headline>

      <StyledParagraphContainer>
        <div dangerouslySetInnerHTML={{ __html: html }} />
      </StyledParagraphContainer>
      

      <Heading className="mt-8 mb-4">My Skills</Heading>
      <SkillList>
        {
          skills.map((skill, i) => (
            <Skill key={i} img_src={skill.img.publicURL} short_name={skill.short_title} name={skill.title} />
          ))
        }
      </SkillList>
    </StyledContainer>
  );
};

About.propTypes = {
  data: PropTypes.array.isRequired,
};

export default About;