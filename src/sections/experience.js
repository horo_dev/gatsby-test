import React from 'react'
import styled from 'styled-components'
import { theme, media, Section, Heading } from '@styles';

const { colors, margin, margins, fontSizes, fonts } = theme;

const ExperienceList = styled.ul`
    display: flex;
    overflow-x: auto;
    list-style-type: none;
    list-style-image: none;
    position: relative;
    flex: auto 0 0;

    margin-left: calc(${margin} * -1);
    width: calc(${margin} * 2 + 100%);
    ${media.lg`
        margin-left: 0;
        margin-right: 1rem;
        width: auto;
        flex-direction: column;

        align-items: flex-start;
        overflow: hidden;


        &::after {
            display: none;
        }
    `};

    &::after {
        content: '';
        position: relative;
        padding-left: ${margin};
    }
    
    span.highlighter {
        height: 2px;
        width: 250px;
        bottom: 6px;
        background-color: ${colors.brand};
        position: absolute;

        transform: translateX(${margin}) translateY(0);
        transition: transform 250ms ease-in-out;

        ${media.lg`
            height: 34%;
            width: 2px;
            bottom: 0;
            top: 0;
            transform: translateX(0) translateY(0);
        `}

        &[data-target='hhu'] {
            transform: translateX(${margin});
            ${media.lg`
                transform: translateX(0) translateY(0);
            `}
        }
        &[data-target='hsnr'] {
            transform: translateX(calc(${margin} + (250px * 1)));
            ${media.lg`
                transform: translateX(0) translateY(100%);
            `}
        }
        &[data-target='eevo'] {
            transform: translateX(calc(${margin} + (250px * 2)));
            ${media.lg`
                transform: translateX(0) translateY(200%);
            `}
        }
    }

    li {
        color: ${colors.paragraph};
        text-align: center;
        border-bottom: 1px solid ${colors.border};
        font-size: ${fontSizes.sm};
        min-width: 250px;
        max-width: 250px;
        flex: 1 1 0;
        
        white-space: nowrap;
        position: relative;
        cursor: pointer;
        
        padding: 0.5rem 1rem;
        margin-bottom: .4em;

        ${media.lg`
            font-size: ${fontSizes.xs};
            text-align: left;
            min-width: 180px;
            max-width: 180px;

            border-bottom-width: 0px;
            border-left-width: 1px;
            margin-bottom: 0;
        `};
    
        &.active {
            color: ${colors.brand};
            background: ${colors.border};
            border-radius: 0 3px 3px 0;
            font-weight: 600;
    
            &::after {
                background-color: ${colors.brand};
                height: 3px;
            }
        }
    
        &:first-child {
            ${media.lg`
                margin-left: 0;
            `}
            margin-left: ${margin};
        }
    }
`;

function selectExperience(e) {
    let neighbours = e.parentNode.children;
    let highlighter = false;
    if (neighbours[neighbours.length - 1].classList.contains('highlighter')) {
        highlighter = neighbours[neighbours.length - 1];
    }
    e.classList.add('active');
    for (const n of neighbours) {
        if (n !== e) {
            n.classList.remove('active');
        }
    }

    const target = e.getAttribute('data-target');
    let potentialTargets = document.getElementsByClassName('targets')[0].children;
    for (const t of potentialTargets) {
        if (t.getAttribute('data-target') === target) {
            if (highlighter !== false) {
                highlighter.setAttribute('data-target', target);
            }
            t.classList.add('active');
        }
        else {
            t.classList.remove('active');
        }
    }
}

const Wrapper = styled.div`
${media.lg`display: flex;`};
display: relative;
align-items: flex-start;
justfiy-content: flex-start;
`;

const StyledTargetList = styled.div`
position: relative;
flex: auto;
`

const StyledTarget = styled.div`
top: 0;
opacity: 0;
visibility: hidden;
position: absolute;

ul { max-width: 60ch; }

&.active {
    visibility: visible;
    opacity: 100%;
    position: relative;
    height: auto;
}

header {
    font-weight: 500;
    color: ${colors.heading};
    margin-bottom: ${margins.lg};
    a {
        color: ${colors.brand};
    }
    small {
        font-family: ${fonts.mono};
    }
}
`

const Experience = ({ data }) => {
    const { frontmatter } = data[0].node;
    const { title, experiences } = frontmatter;

    return (
        <Section>
            <Heading className="mb-2">{title}</Heading>
            <Wrapper>
                <ExperienceList className='selectors'>
                    {
                        experiences.map(({ location, short_location }, i) => 
                            <li key={i} onClick={(e) => selectExperience(e.target)} data-target={short_location} className={i > 0 ? '' : 'active'}>
                                {location}
                            </li>
                        )
                    }
                    <span className='highlighter'></span>
                </ExperienceList>
                <StyledTargetList className='targets'>
                    {
                        experiences.map(({ location, short_location, url, occupation_title, timespan, summary }, i) => 
                            <StyledTarget key={i} data-target={short_location} className={i > 0 ? 'target' : 'target active'}>
                                <header>
                                    <h2>{occupation_title} <a href={url}>@{location}</a></h2>
                                    <small>{timespan}</small>
                                </header>
                                <ul>
                                    {
                                        summary.map((item, i) => (
                                            <li key={i}>{item}</li>
                                        ))
                                    }
                                </ul>
                            </StyledTarget>
                        )
                    }
                </StyledTargetList>
            </Wrapper>
        </Section>
    )
}
export default Experience