
module.exports = {
    siteTitle: "Tim Nelke",
    siteDescription: "Tim Nelke is a computer science student based in Willich, Germany who is striving to become a great software engineer.",
    keywords: "",
    siteUrl: "https://timnelke.com",
    language: "en",

    twitterHandle: "@horo_dev",
    socialMedia: [
        {
            name: "GitHub",
            url: "https://github.com/itshoro"
        },
        {
            name: "Twitter",
            url: "https://twitter.com/horo_dev"
        }
    ],
    backgroundColor: "#FFFFFF",
    themeColor: "#171717",
}