import theme from './theme'
import { css } from 'styled-components'

const { sizes } = theme;

export const media = Object.keys(sizes).reduce((acc, size) => {
    const sizeInEms = sizes[size] / 16;
    acc[size] = (...args) => css`
        @media (min-width: ${sizeInEms}em) {
            ${css(...args)}
        }
    `;
    return acc;
}, {});

export default media;