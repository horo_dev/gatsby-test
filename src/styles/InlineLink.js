import styled from "styled-components"
import theme from "./theme"
const { colors, transition } = theme;

const InlineLink = styled.a`
  display: inline-block;
  text-decoration: none;
  text-decoration-skip-ink: auto;
  position: relative;
  transition: ${transition};
  cursor: pointer;

  &:hover,
  &:focus,
  &:active {
    colors: ${colors.brand};
    outline: 0;
    transform: scale(1);
  }

  &::after {
    content: '';
    display: block;
    height: 1px;
    width: 100%;
    bottom: 0.37em;
    transform: scale(0);
    background-color: ${colors.brand};
  }
`;

export default InlineLink;