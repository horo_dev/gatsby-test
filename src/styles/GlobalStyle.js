import { createGlobalStyle } from "styled-components"
import theme from "./theme"
import media from "./media"
import FontFaces from "./fonts"

const { colors, fonts, fontSizes, margin, margins } = theme

const GlobalStyle = createGlobalStyle`
${FontFaces}
/* http://meyerweb.com/eric/tools/css/reset/ 
v2.0 | 20110126
License: none (public domain)
*/

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  font-size: 100%;
  font: inherit;
  vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
  display: block;
}
body {
  line-height: 1;
}
ol, ul {
  list-style: none;
}
blockquote, q {
  quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
  content: '';
  content: none;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
}


html {
  box-sizing: border-box;
  width: 100%;
  font-family: ${fonts.sans_serif};
}

*,
*:before,
*:after {
  box-sizing: inherit;
}

:root {
  --margin: 2rem;
  ${media.sm`--margin: 4rem`};
  ${media.md`--margin: 2.5rem`};
  ${media.lg`--margin: 8rem`};
}

body {
  width: 100%;
  min-height: 100%;
  overflow-x: hidden;
  -moz-osx-font-smoothing: grayscale;
  -webkit-font-smoothing: antialiased;
  font-weight: normal;
  word-wrap: break-word;
  font-kerning: normal;
  -moz-font-feature-settings: "kern", "liga", "clig", "calt";
  -ms-font-feature-settings: "kern", "liga", "clig", "calt";
  -webkit-font-feature-settings: "kern", "liga", "clig", "calt";
  font-feature-settings: "kern", "liga", "clig", "calt";
  
  line-height: 1.3;
}

::selection {
  background-color: ${colors.brand};
}


h1,
h2,
h3,
h4,
h5,
h6 {
  font-weight: 600;
  color: ${colors.heading};
  margin: 0 0 10px 0;
}

h1 {
  &.big-title {
    font-size: 80px;
    line-height: 1.1;
    margin: 0;
    ${media.xl`font-size: 70px`};
    ${media.lg`font-size: 60px`};
    ${media.md`font-size: 50px`};
    ${media.sm`font-size: 40px`};
  }
  
  &.medium-title {
    font-size: 60px;
    line-height: 1.1;
    margin: 0;
    ${media.xl`font-size: 50px`};
    ${media.md`font-size: 40px`};
  }
}

img {
  width: 100%;
  max-width: 100%;
  vertical-align: middle;
}

img[alt=""],
img:not([alt]) {
  filter: blur(5px);
}

a {
  display: inline-block;
  text-decoration: none;
  text-decoration-skip-ink: auto;
  color: inherit;
  position: relative;
  cursor: pointer;
  
  &:hover,
  &:focus {
    color: ${colors.brand};
  }
}

pre,
code {
  font-family: ${fonts.mono};
}

svg {
  height: 100%;
  fill: currentColor;
  vertical-align: middle;
}

p, li {
  max-width: 60ch;
}

.logo {
  color: ${colors.brand};
  fill: currentColor;
  height: 1rem;
}

.link {
  cursor: pointer;
  margin-right: ${margins.sm};
  padding-top: ${margins.xs};
  font-size: ${fontSizes.sm};
  
  positon: relative;
  display: flex;
  align-items: center;
  list-style-type: none;
  color: ${colors.brand};
}

.link svg {
  margin-right: ${margins.xs};
}
.link::before {
  content: "";
  position: absolute;
  bottom: 0;
  left: 0;
  z-index: -1;
  
  height: 100%;
  width: 100%;
  
  background-color: ${colors.border};
  
  transform: scaleY(0.1);
  transform-origin: bottom center;
  
  transition: 350ms transform cubic-bezier(0.32, 1.28, 0.18, 0.89);
}
.link:hover::before {
  transform: scaleY(.8);
}

.filter-brand {
  background-color: ${colors.brand};
}
.filter-brand img {
  display: block;
  mix-blend-mode: darken;
}

.logo svg {
  height: 2rem;
}


.m-base {
  margin: ${margin};
}

.mx-base {
  margin-left: ${margin};
  margin-right: ${margin};
}

.p-base {
  padding: ${margin};
}

.px-base {
  padding-left: ${margin};
  padding-right: ${margin};
}

.border {
  border: 1px solid ${colors.border};
}

.border-b {
  border-bottom: 1px solid ${colors.border};
}

.border-l {
  border-left: 1px solid ${colors.border};
}

.rounded-full {
  border-radius: 50%;
}
.rounded {
  border-radius: 3px;
}
.rounded-md {
  border-radius .25em;
}
`

export default GlobalStyle
