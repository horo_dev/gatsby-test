import styled from "styled-components"
import theme from "./theme"

const { margin } = theme;

const Section = styled.section`
  margin: 2rem auto;
  padding-left: ${margin};
  padding-right: ${margin};
`;

export default Section;