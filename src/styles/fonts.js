import { css } from "styled-components"

import SenRegularTTF from "@fonts/Sen/sen-v1-latin-regular.ttf"
import SenRegularWOFF from "@fonts/Sen/sen-v1-latin-regular.woff"
import SenRegularWOFF2 from "@fonts/Sen/sen-v1-latin-regular.woff2"

import SenMediumTTF from "@fonts/Sen/sen-v1-latin-700.ttf"
import SenMediumWOFF from "@fonts/Sen/sen-v1-latin-700.woff"
import SenMediumWOFF2 from "@fonts/Sen/sen-v1-latin-700.woff2"

const FontFaces = css`
  @font-face {
    font-family: 'Sen';
    font-weight: 400;
    font-style: normal;
    src: url(${SenRegularWOFF2}) format('woff2'), url(${SenRegularWOFF}) format('woff'),
      url(${SenRegularTTF}) format('truetype');
  }

  @font-face {
    font-family: 'Sen';
    font-weight: 700;
    font-style: normal;
    src: url(${SenMediumWOFF2}) format('woff2'), url(${SenMediumWOFF}) format('woff'),
      url(${SenMediumTTF}) format('truetype');
  }
`;

export default FontFaces;