const theme = {
    sizes: {
        sm: '640',
        md: '768',
        lg: '1024',
        xl: '1280'
    },

    colors: {
        heading: '#000',
        paragraph: '#a0aec0',
        border: '#e2e8f0',
        brand: '#f20c3a',
    },

    fontSizes: {
        xs: '.75rem',
        sm: '.875rem',
        base: '1rem',
        lg: '1.125rem',
        xl: '1.25rem',
        xxl: '1.5rem',
        xxxl: '1.875rem',
        xxxxl: '2.25rem',
        xxxxxl: '3rem',
        xxxxxxl: '4rem',
    },

    margin: 'var(--margin, 2rem)',

    margins: {
        none: '0',
        xs: '0.5em',
        sm: '1em',
        base: '2em',
        lg: '4em',
        xl: '3em',
        xxl: '4em',
        xxxl: '6em'
    },

    fonts: {
        sans_serif: 'Sen, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif',
        mono: 'Consolas, Cascadia Code, SF Mono, Fira Code, Fira Mono, Roboto Mono, Lucida Console, Monaco, monospace'
    },


    transition: 'all 250ms cubic-bezier(0.32, 1.28, 0.18, 0.89);'
};

export default theme;