import styled from 'styled-components';
import theme from './theme';
import { media } from './media';

const { colors, fontSizes, fonts, margins } = theme;

const Heading = styled.h2`
  position: relative;
  display: flex;
  font-family: ${fonts.sans_serif};
  font-weight: 500;
  color: ${colors.heading};
  align-items: center;

  margin-top: var(--margin);

  ${media.md`font-size: ${fontSizes.lg};`};

  &::after {
    content: '';
    flex: 1;
    margin-left: .5em;
    height: 1px;
    background-color: ${colors.border};
  }
`;

export default Heading;