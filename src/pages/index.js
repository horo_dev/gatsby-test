import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"
import { graphql } from "gatsby";

import About from "@components/sections/about"
import Experience from "@components/sections/experience"
import Skills from "@components/sections/skills"
import Featured from "@components/sections/featured"

import Layout from "@components/layout"
import SocialContainer from "@components/social"
import LegalContainer from "@components/legal"

import { theme, media, Section } from "@styles"
const { colors, margins, margin } = theme;

const GridContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(1, minmax(0, 1fr));
  gap: 2.5rem;

  ${media.lg`grid-template-columns: repeat(2, minmax(0, 1fr));`};
`

const FlexContainer = styled.footer`
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: flex-start;
  list-style: none;


  padding-top: ${margins.sm};
  padding-bottom: .5em;

  border-top: 1px solid ${colors.border};
  display: none;

  ${media.md`display: flex;`}
`

const StickyAside = styled.aside`
  align-self: start;
  top: 0;
  ${media.lg`position: sticky;`}
`

const StyledMain = styled.main`
  margin-left: ${margin};
  margin-right: ${margin};
`

const IndexPage = ({ data }) => {
  return (
    <Layout>
      <GridContainer>
        <StickyAside>
          <Section>
            <About data={data.about.edges}/>
            <FlexContainer>
              <LegalContainer />
              <SocialContainer />
            </FlexContainer>
          </Section>
        </StickyAside>
        <StyledMain>
          <Skills data={data.about.edges} />
          <Experience data={data.experience.edges}/>
          <Featured data={data.featured.edges}/>
        </StyledMain>
      </GridContainer>
    </Layout>
  );
} 

// https://tailwindcss.com/docs/customizing-colors/#naming-your-colors
IndexPage.propTypes = {
  data: PropTypes.object.isRequired
};

export const pageQuery = graphql`
{
  about: allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/about/"}}) {
     edges {
       node {
         id
         frontmatter {
          title
          name
          subtitle
          avatar {
            childImageSharp {
              fixed(width: 96, quality: 90, traceSVG: { color: "#610f3c" }) {
                ...GatsbyImageSharpFixed_withWebp_tracedSVG
              }
            }
          }
          skills {
            title
            short_title
            img {
              publicURL
            }
          }
        }
        html
      }
    }
  }
  experience: allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/experience/"}}) {
    edges {
      node {
        id
        frontmatter {
          title
          experiences {
            location
            short_location
            url
            occupation_title
            timespan
            summary
          }
        }
      }
    }
  }
  featured: allMarkdownRemark(filter: {fileAbsolutePath: {regex: "/featured/"}}) {
    edges {
      node {
        id
        frontmatter {
          title
          projects {
            title
            tags
            url
            description
            img {
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
  }
}
`;

export default IndexPage;