import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import config from '@config';
import favicon from '@images/favicons/favicon.ico';
import ogImage from '@images/favicons/og.png';
import appleIcon180x180 from '@images/favicons/apple-touch-icon.png';
import androidIcon192x192 from '@images/favicons/android-chrome-192x192.png';
import androidIcon512x512 from '@images/favicons/android-chrome-512x512.png';
import favicon16x16 from '@images/favicons/favicon-16x16.png';
import favicon32x32 from '@images/favicons/favicon-32x32.png';
import msIcon144x144 from '@images/favicons/mstile-150x150.png';

const Head = ({ metadata }) => (
  <Helmet>
    <html lang="en" prefix="og: http://ogp.me/ns#" />
    <title itemProp="name" lang="en">
      {metadata.title}
    </title>
    <link rel="shortcut icon" href={favicon} />
    <link rel="canonical" href="https://timnelke.com" />

    <meta name="description" content={metadata.description} />
    <meta name="keywords" content={config.keywords} />
    <meta property="og:title" content={metadata.title} />
    <meta property="og:description" content={metadata.description} />
    <meta property="og:type" content="website" />
    <meta property="og:url" content={metadata.url} />
    <meta property="og:site_name" content={metadata.title} />
    <meta property="og:image" content={`${config.url}${ogImage}`} />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:locale" content={config.language} />
    <meta itemProp="name" content={metadata.title} />
    <meta itemProp="description" content={metadata.description} />
    <meta itemProp="image" content={`${config.url}${ogImage}`} />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:url" content={metadata.url} />
    <meta name="twitter:site" content={config.twitterHandle} />
    <meta name="twitter:creator" content={config.twitterHandle} />
    <meta name="twitter:title" content={metadata.title} />
    <meta name="twitter:description" content={metadata.description} />
    <meta name="twitter:image" content={`${config.url}${ogImage}`} />
    <meta name="twitter:image:alt" content={metadata.title} />

    <link rel="apple-touch-icon" sizes="180x180" href={appleIcon180x180} />
    <link rel="icon" type="image/png" sizes="192x192" href={androidIcon192x192} />
    <link rel="icon" type="image/png" sizes="512x512" href={androidIcon512x512} />
    <link rel="icon" type="image/png" sizes="16x16" href={favicon16x16} />
    <link rel="icon" type="image/png" sizes="32x32" href={favicon32x32} />
    <link rel="manifest" href="/assets/favicon/site.webmanifest" />
    <link rel="shortcut icon" href={favicon}/>
    <meta name="msapplication-TileColor" content={config.theme_color} />
    <meta name="msapplication-TileImage" content={msIcon144x144} />
    <meta name="theme-color" content={config.theme_color} />
  </Helmet>
);

export default Head;

Head.propTypes = {
  metadata: PropTypes.object.isRequired,
};