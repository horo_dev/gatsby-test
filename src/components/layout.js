import React from "react"
import styled from "styled-components"
import { StaticQuery, graphql } from "gatsby"

import { GlobalStyle, theme, media } from "@styles"
import { Logo } from "@components/icons"
import Head from "@components/head"
import Legal from "@components/legal"
import Social from "@components/social"
const { margin, margins, colors, fontSizes } = theme;


const StyledContent = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

const StyledFlex = styled.div`
  display: flex;

  ${media.md`display: none;`};
`

const LogoContainer = styled.div`
  margin-top: ${margins.xxl};
  margin-bottom: ${margins.base};

  margin-left: ${margin};

  & > a {
    display: block;
    width: 1.2rem;
    height: auto;
    color: ${colors.brand};
    fill: currentColor;
  }
`;

const StyledFooter = styled.footer`
  margin-left: ${margin};
  margin-right: ${margin};

  margin-bottom: ${margins.lg};
`

const StyledNotice = styled.p`
  color: ${colors.paragraph};
  font-size: ${fontSizes.xs};

  span {
    font-weight: 700;
  }
`

const Layout = ({ children }) => {
  return (
    <StaticQuery
      query={graphql`
        {
          site {
            siteMetadata {
              title
              siteUrl
              description
            }
          }
        }
      `}
      render={({ site }) => (
        <div id="root">

          <Head metadata={site.siteMetadata} />
          <GlobalStyle />

          <StyledContent>
            
            <div id="content">
              <LogoContainer>
                <a className="logo" href="/">
                  <Logo />
                </a>
              </LogoContainer>
              {children}
              
              <StyledFooter>
                <StyledFlex>
                  <Legal />
                  <Social />
                </StyledFlex>
                <StyledNotice>
                  Developed and Designed by <span>&copy; Tim Nelke {new Date().getFullYear()}</span>
                </StyledNotice>
              </StyledFooter>
            </div>
          </StyledContent>
        </div>
      )}
    />
  )
}

export default Layout
