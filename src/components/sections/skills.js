import React from "react"
import styled from "styled-components"
import { media, theme, Heading } from "@styles"

const { colors, margins } = theme;

const Skill = ({ img_src, short_name, name }) => (
    <SkillItem className="border rounded-md">
      <img src={img_src} alt={`Logo of ${short_name}`} />
      {name}
    </SkillItem>
)

const SkillItem = styled.li`
border-color: ${colors.border};
padding: 1rem;
display: flex;
flex: 1 1 0%;
white-space: nowrap;
align-items: center;

${media.lg`
justify-content: center;
  flex-direction: column;
  overflow: hidden;
`};

img {
  display: block;
  filter: grayscale(1);
  transition: filter 150ms ease-out;

  width: 3em;
  height: auto;
  border-right: 1px solid ${colors.border};


  padding-right: ${margins.sm};
  margin-right: ${margins.xs};

  ${media.lg`
    padding-right: 0;
    margin-right: 0;
    border-right: none;

    width: auto;
    height: 2.5em;

    border-bottom 1px solid ${colors.border};
    padding-bottom: ${margins.sm};
    margin-bottom: ${margins.xs};
  `}
}

&:hover img, &:focus img {
  filter: grayscale(0);
}

`
const SkillList = styled.ul`
  display: grid;
  grid-template-columns: repeat(1, minmax(0, 1fr));
  grid-template-rows: min-content;
  align-items: start;
  padding: 0;
  gap: ${margins.xs};

  ${media.md`
    gap: ${margins.sm};
    grid-template-columns: repeat(3, minmax(0, 1fr));
  `};
`

const Skills = ({ data }) => {
    const { frontmatter } = data[0].node;
    const { skills } = frontmatter;
    
    return (
        <section>
            <Heading className="mt-8 mb-4">My Skills</Heading>
            <SkillList>
            {
                skills.map((skill, i) => (
                <Skill key={i} img_src={skill.img.publicURL} short_name={skill.short_title} name={skill.title} />
                ))
            }
            </SkillList>
        </section>
    );
} 

export default Skills;