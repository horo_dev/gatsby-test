import React from "react"
import styled from "styled-components"
import { Heading, media, theme } from "@styles"
import Img from "gatsby-image"

const { margins } = theme;
const StyledGrid = styled.div`
display: grid;
grid-template-columns: repeat(1, minmax(0, 1fr));
gap: ${margins.sm};

${media.lg`grid-template-columns: repeat(2, minmax(0, 1fr));`}
`;

const GridItem = styled.div`
overflow: hidden;
border-radius: 3px;
`;

const FeaturedContainer = ({ data }) => {
    const { frontmatter } = data[0].node;
    const { title, projects } = frontmatter;

    return (
        <>
            <Heading>{title}</Heading>

            <StyledGrid>
                {
                    projects.map(({ title, tags, url, img, description }, i) => (
                        <GridItem>
                            <Img fluid={img.childImageSharp.fluid} alt={title}/>
                            {title}
                            {description}
                            <a href={url}>Learn More</a>
                        </GridItem>
                    ))
                }
            </StyledGrid>
        </>
    );
};

export default FeaturedContainer;