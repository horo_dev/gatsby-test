import React from 'react'
import styled from 'styled-components'
import { theme, media, Heading } from '@styles';

const { colors, margin, margins, fontSizes, fonts } = theme;

const ExperienceList = styled.ul`
    display: flex;
    overflow-x: auto;
    list-style-type: none;
    list-style-image: none;
    position: relative;
    flex: auto 0 0;

    margin-top: ${margins.base};
    margin-bottom: ${margins.sm};

    margin-left: calc(${margin} * -1);
    width: calc(${margin} * 2 + 100%);
    ${media.lg`
        margin-left: 0;
        width: auto;
        overflow-x: hidden;

        flex: 1 0 0;

        &::after {
            display: none;
        }
    `};

    &::after {
        content: '';
        position: relative;
        padding-left: ${margin};
    }
    
    span.highlighter {
        height: 2px;
        width: 250px;
        bottom: 6px;
        background-color: ${colors.brand};
        position: absolute;

        transform: translateX(${margin});
        transition: transform 250ms ease-in-out;

        ${media.lg`
            transform: translateX(0);

            width: 33.334% ;
            height: 2px;

            bottom: 0;
        `}

        &[data-target='hhu'] {
            transform: translateX(${margin});
            ${media.lg`
                transform: translateX(0);
            `}
        }
        &[data-target='hsnr'] {
            transform: translateX(calc(${margin} + (250px * 1)));
            ${media.lg`
                transform: translateX(calc(100%));
            `}
        }
        &[data-target='eevo'] {
            transform: translateX(calc(${margin} + (250px * 2)));
            ${media.lg`
                transform: translateX(calc(200%));
            `}
        }
    }

    li {
        color: ${colors.paragraph};
        text-align: center;
        border: 1px solid ${colors.border};
        border-color: transparent transparent ${colors.border} transparent;
        font-size: ${fontSizes.sm};
        min-width: 250px;
        max-width: 250px;
        flex: 1 1 0;
        
        white-space: nowrap;
        position: relative;
        cursor: pointer;
        
        padding: 0.5rem 1rem;
        margin-bottom: .4em;

        ${media.lg`
            margin-bottom: 0;

            min-width: unset;
            max-width: unset;
        `};
    
        &.active {
            color: ${colors.brand};
            border-color: ${colors.border};
            border-radius: 0 3px 3px 0;
            font-weight: 600;
    
            &::after {
                background-color: ${colors.brand};
                height: 3px;
            }
        }
    
        &:first-child {
            ${media.lg`
                margin-left: 0;
            `}
            margin-left: ${margin};
        }
    }
`;

function selectExperience(e) {
    let neighbours = e.parentNode.children;
    let highlighter = false;
    if (neighbours[neighbours.length - 1].classList.contains('highlighter')) {
        highlighter = neighbours[neighbours.length - 1];
    }
    e.classList.add('active');
    for (const n of neighbours) {
        if (n !== e) {
            n.classList.remove('active');
        }
    }

    const target = e.getAttribute('data-target');
    let potentialTargets = document.getElementsByClassName('targets')[0].children;
    for (const t of potentialTargets) {
        if (t.getAttribute('data-target') === target) {
            if (highlighter !== false) {
                highlighter.setAttribute('data-target', target);
            }
            t.classList.add('active');
        }
        else {
            t.classList.remove('active');
        }
    }
}

const Wrapper = styled.div`
display: relative;
align-items: flex-start;
justfiy-content: flex-start;
`;

const StyledTargetList = styled.div`
position: relative;
flex: auto;
`

const StyledTarget = styled.div`
top: 0;
opacity: 0;
visibility: hidden;
position: absolute;

ul { max-width: 60ch; }

&.active {
    visibility: visible;
    opacity: 100%;
    position: relative;
    height: auto;
}

header {
    font-weight: 500;
    color: ${colors.heading};
    margin-bottom: ${margins.lg};
    a {
        color: ${colors.brand};
    }
    small {
        font-family: ${fonts.mono};
    }
}
`

const StyledSummaryItem = styled.li`
    color: ${colors.paragraph};
    display: block;
    margin-bottom: ${margins.sm};

    &::before {
        content: "→ ";
        left: 0;
        color: ${colors.brand};
    }
`

const StyledSummaryList = styled.ul`
    list-style: none;
`

const Experience = ({ data }) => {
    const { frontmatter } = data[0].node;
    const { title, experiences } = frontmatter;

    return (
        <>
            <Heading className="mb-2">{title}</Heading>
            <Wrapper>
                <ExperienceList className='selectors'>
                    {
                        experiences.map(({ location, short_location }, i) => 
                            <li key={i} onClick={(e) => selectExperience(e.target)} data-target={short_location} className={i > 0 ? '' : 'active'}>
                                {location}
                            </li>
                        )
                    }
                    <span className='highlighter'></span>
                </ExperienceList>
                <StyledTargetList className='targets'>
                    {
                        experiences.map(({ location, short_location, url, occupation_title, timespan, summary }, i) => 
                            <StyledTarget key={i} data-target={short_location} className={i > 0 ? 'target' : 'target active'}>
                                <header>
                                    <h2>{occupation_title} <a href={url}>@{location}</a></h2>
                                    <small>{timespan}</small>
                                </header>
                                <StyledSummaryList>
                                    {
                                        summary.map((item, i) => (
                                            <StyledSummaryItem key={i}>{item}</StyledSummaryItem>
                                        ))
                                    }
                                </StyledSummaryList>
                            </StyledTarget>
                        )
                    }
                </StyledTargetList>
            </Wrapper>
        </>
    )
}
export default Experience