import React from "react"
import PropTypes from "prop-types"
import Img from "gatsby-image"
import styled from "styled-components"
import { theme, media, Heading } from "@styles"

const { colors, margins, fontSizes, fonts } = theme;

const StyledParagraphContainer = styled.div`
margin-top: ${margins.xl};
margin-bottom: ${margins.lg};
p:not(:last-of-type) {
  margin-bottom: ${margins.sm};
}
a {
  color: ${colors.heading};
  font-weight: 500;
  position: relative;
  &::before {
    height: 100%;
    width: 100%;
    background-color: ${colors.border};
    position: absolute;
    bottom: 0;
    left: 0;
    content: "";
    z-index: -1;
    transform: scaleY(0.1);
    transform-origin: bottom center;

    transition: 350ms transform cubic-bezier(0.32, 1.28, 0.18, 0.89);
  }

  &:hover {
    color: ${colors.brand};
    &::before {
      transform: scaleY(.8);
    }
  }
}
`

const Headline = styled.div`
  color: ${colors.heading};
  margin-top: ${margins.lg};
  margin-bottom: ${margins.sm};

  & > :first-child {
    margin-bottom: ${margins.base};
  }
`

const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;

  color: ${colors.paragraph};
  padding-bottom: ${margins.xxl};
  padding-top: ${margins.lg};
`

const StyledOverline = styled.h1`
font-size: ${fontSizes.xxl};

font-weight: 500;

`

const StyledName = styled.span`
color: ${colors.brand};
font-weight: 700;
`

const StyledSubtitle = styled.h3`
font-size: ${fontSizes.lg};
font-weight: 400;
`

const About = ({ data }) => {
  const { frontmatter, html } = data[0].node;
  const { title, name, subtitle, avatar } = frontmatter;
  
  return (
    <StyledContainer>
      <Headline>
        <Img className="rounded-full filter-brand" fixed={avatar.childImageSharp.fixed} alt="of Tim Nelke"></Img>
        <StyledOverline>{title} <StyledName>{name}</StyledName>.</StyledOverline>
        <StyledSubtitle>{subtitle}</StyledSubtitle>
      </Headline>

      <StyledParagraphContainer>
        <div dangerouslySetInnerHTML={{ __html: html }} />
      </StyledParagraphContainer>
      
    </StyledContainer>
  );
};

About.propTypes = {
  data: PropTypes.array.isRequired,
};

export default About;