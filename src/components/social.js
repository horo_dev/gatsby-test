import React from "react"
import styled from "styled-components"

import { socialMedia } from "@config"

import { FormattedIcon } from "@components/icons"

const StyledList = styled.ul`
display: flex;
padding: 0;

li {
    list-style: none;
}
`

const SocialContainer = () => (
    <StyledList>
        {socialMedia && socialMedia.map(({name, url}, i) => (
            <li key={i}>
                <a className="link" href={url} rel="nofollow noopener norefferrer" aria-label={name}> <FormattedIcon type={name}/>
                    {name}
                </a>
            </li>
        ))}
    </StyledList>
);

export default SocialContainer;