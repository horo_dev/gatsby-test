import GitHubIcon from "./github"
import TwitterIcon from "./twitter"
import FormattedIcon from "./formattedIcon"
import Logo from "./logo"


export { 
    GitHubIcon, 
    TwitterIcon,
    Logo,
    FormattedIcon
}