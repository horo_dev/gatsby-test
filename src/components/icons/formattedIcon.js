import React from "react"
import {
    TwitterIcon,
    GitHubIcon,
    Logo
} from ".";

const FormattedIcon = ({type}) => {
    type = type.toLowerCase();
    switch (type) {
        case "twitter":
            return <TwitterIcon/>
        case "github":
            return <GitHubIcon />
        default:
            return <Logo />
    }
}

export default FormattedIcon