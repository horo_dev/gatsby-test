---
title: Education and Experience
experiences: [
    {
        location: Heinrich Heine University,
        short_location: hhu,
        url: "https://www.uni-duesseldorf.de/",
        occupation_title: Student,
        timespan: Oct 2017 — Sep 2018,
        summary: [
            "Learned Java and best practices of object orriented programming",
            "Populating databases and database algebra",
            "Theoretical knowledge of multiple algorithms, for e.g. text search or data storage"
        ]
    },
    {
        location: Hochschule Niederrhein,
        short_location: hsnr,
        url: "https://www.hs-niederrhein.de/",
        occupation_title: Student,
        timespan: Oct 2018 — Present,
        summary: [
            "Developing applcations using C, C++ and python, improving knowledge of object orriented programming and its use",
            "Multithreaded programming, and distributed systems",
            "Image processing using OpenCV"
        ]
    },
    {
        location: eEvolution GmbH,
        short_location: eevo,
        url: "https://www.eevolution.de/",
        occupation_title: Programmer,
        timespan: Jan 2018 — Mar 2018,
        summary: [
            "Initial experience with enterprise software development",
            "Scrum events & presentation of software fixes to team members",
            "Version control (Microsoft TSF)"
        ]
    }
]
---