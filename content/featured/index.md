---
title: Featured Projects
projects: [
    {
        title: timnelke.com,
        tags: [ "HTML", "(S)CSS", "JavaScript", "React" ],
        url: "https://github.com/itshoro/timnelke.com",
        img: "./images/timnelke.png",
        description: "The third iteration of my personal website, built using GatsbyJS and React and an own CSS framework inspired by tailwind-css."
    },
    {
        title: Basho,
        tags: [ "python", "HTML", "CSS" ],
        url: "https://github.com/itshoro/basho",
        img: "./images/shapeGenerator.png",
        description: "Basho is meant to be a distributed pax-counter, however currently it only retrieves device data, and gives no real
        insight on the current actual status."
    },
    {
        title: Lazer DB Map Converter,
        tags: [ "C#" ],
        url: "https://github.com/itshoro/timnelke.com",
        img: "./images/lazer-db-conv.png",
        description: "Command Line Tool that converts the osu!lazer database to a regular song directory structure, to be used by the regular osu! client."
    },
    {
        title: MTag,
        tags: [ "C#", ".NET Core" ],
        url: "https://github.com/itshoro/timnelke.com",
        img: "./images/mtag.png",
        description: "A .Net Core Library to extract Metadata from Audio files. Currently only simple support for ID3v2 Headers."
    }
]
---