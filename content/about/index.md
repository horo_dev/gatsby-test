---
title: 'Ohayo, my name is'
name: 'Tim Nelke'
subtitle: I'm a computer science major that gets code done.
avatar: './me.jpg'
skills:
  [
    {
      title: 'python',
      short_title: 'python',
      img: './images/python-logo-generic.svg'
    },
    {
      title: '.NET Core',
      short_title: '.NET Core',
      img: './images/net-core-logo.svg' 
    },
    {
      title: 'HTML & S(CSS)',
      short_title: 'HTML and SCSS',
      img: './images/sass-logo-color.svg' 
    },
    {
      title: 'Javascript (ES6+)',
      short_title: 'Javascript',
      img: './images/js-logo-generic.svg' 
    },
    {
      title: 'UI/UX Design', 
      short_title: 'UI and UX Design',
      img: './images/figma-logo-icon.svg' 
    }
  ]
---
Currently I'm in my fourth semester at [Hochschule Niederrhein](https://hs-niederrhein.de/), in Germany.

While I might still be a novice looking at my current skillset, I'm striving to become a kick-ass software engineer.